﻿using ExampleApp.Database;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExampleApp.Repository
{
    public class BaseRepository
    {
        public ExampleAppDbContext dc = new ExampleAppDbContext();

        ~BaseRepository()
        {
            if (dc != null)
                dc.Dispose();
        }
    }
}
