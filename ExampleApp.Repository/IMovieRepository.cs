﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ExampleApp.Domain.Models;

namespace ExampleApp.Repository
{
    public interface IMovieRepository
    {
        IEnumerable<Movie> Get();

        IEnumerable<Movie> Get(string search);

        MovieCopy FindCopyOnStock(int movieId);

        void InsertMovieCopyClient(MovieCopyClient movieCopyClient);

        MovieCopyClient GetMovieCopyClient(int movieCopyId, int clientId);

        MovieCopy GetMovieCopy(int movieCopyId);

        void UpdateMovieCopyClient(MovieCopyClient movieCopyClient);

        void UpdateMovieCopy(MovieCopy movieCopy);

        MovieCopyClient GetClientCopy(int id);
    }
}
