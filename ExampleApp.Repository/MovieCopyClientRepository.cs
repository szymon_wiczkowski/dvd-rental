﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ExampleApp.Domain.Models;

namespace ExampleApp.Repository
{
    public class MovieCopyClientRepository : BaseRepository, IMovieCopyClientRepository
    {
        public IEnumerable<MovieCopyClient> Get(int clientId, DateTime? backDate)
        {
            return dc.MoviesCopyClients.Where(c =>
                c.ClientId.Equals(clientId) &&
                c.BackDate.Equals(backDate));
        }
    }
}
