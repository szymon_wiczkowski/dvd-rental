﻿using ExampleApp.Domain.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Data.Entity;
using System.Text;
using System.Threading.Tasks;

namespace ExampleApp.Repository
{
    public class MovieRepository : BaseRepository, IMovieRepository
    {
        public MovieCopy FindCopyOnStock(int movieId)
        {
            return dc.MoviesCopies.FirstOrDefault(m => m.MovieId.Equals(movieId) && m.IsOnStock);
        }

        public IEnumerable<Movie> Get()
        {
            return dc.Movies;
        }

        public IEnumerable<Movie> Get(string search)
        {
            return dc.Movies
                .Where(m =>
                    m.Title.ToLower().Contains(search) ||
                    m.MovieCopies.Any(c => c.SerialNumber.ToLower().Contains(search)));
        }

        public MovieCopyClient GetClientCopy(int id)
        {
            return dc.MoviesCopyClients
                .Include(m=>m.MovieCopy)
                .Include(m => m.Client)
                .Single(c => c.Id.Equals(id));
        }

        public MovieCopy GetMovieCopy(int movieCopyId)
        {
            return dc.MoviesCopies.Single(m => m.Id.Equals(movieCopyId));
        }

        public MovieCopyClient GetMovieCopyClient(int movieCopyId, int clientId)
        {
            return dc.MoviesCopyClients
                .Include(m => m.MovieCopy)
                .Include(m => m.Client)
                .Single(m => m.MovieCopyId.Equals(movieCopyId) && m.ClientId.Equals(clientId));
        }

        public void InsertMovieCopyClient(MovieCopyClient movieCopyClient)
        {
            dc.MoviesCopyClients.Add(movieCopyClient);
            dc.Entry(movieCopyClient).State = System.Data.Entity.EntityState.Added;
            dc.SaveChanges();
        }

        public void UpdateMovieCopy(MovieCopy movieCopy)
        {
            dc.Entry(movieCopy).State = EntityState.Modified;
            dc.SaveChanges();
        }

        public void UpdateMovieCopyClient(MovieCopyClient movieCopyClient)
        {
            dc.Entry(movieCopyClient).State = EntityState.Modified;
            dc.SaveChanges();
        }
    }
}
