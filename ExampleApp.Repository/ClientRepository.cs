﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ExampleApp.Domain.Models;

namespace ExampleApp.Repository
{
    public class ClientRepository : BaseRepository, IClientRepository
    {
        public IEnumerable<Client> Get()
        {
            return dc.Clients;
        }

        public IEnumerable<Client> Get(string search)
        {
            return dc.Clients
                .Where(c =>
                c.FirstName.ToLower().Contains(search) ||
                c.LastName.ToLower().Contains(search) ||
                c.Telephone.ToLower().Contains(search));
        }

        public Client Get(int clientId)
        {
            return dc.Clients.Single(c => c.Id.Equals(clientId));
        }
    }
}
