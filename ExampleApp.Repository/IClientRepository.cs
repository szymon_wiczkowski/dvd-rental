﻿using ExampleApp.Domain.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExampleApp.Repository
{
    public interface IClientRepository
    {
        IEnumerable<Client> Get();

        Client Get(int clientId);

        IEnumerable<Client> Get(string search);
    }
}
