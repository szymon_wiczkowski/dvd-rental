﻿using ExampleApp.Domain.Models;
using System;
using System.Collections.Generic;

namespace ExampleApp.Repository
{
    public interface IMovieCopyClientRepository
    {
        IEnumerable<MovieCopyClient> Get(int clientId, DateTime? backDate);
    }
}