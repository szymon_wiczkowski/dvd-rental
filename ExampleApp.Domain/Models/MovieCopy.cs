﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExampleApp.Domain.Models
{
    public class MovieCopy
    {
        public int Id { get; set; }

        public int MovieId { get; set; }

        public virtual Movie Movie { get; set; }

        [Required]
        [Index(IsUnique = true)]
        [StringLength(100)]
        public string SerialNumber { get; set; }

        public bool IsOnStock { get; set; }

        public DateTime BuyDate { get; set; }

        public DateTime? TrashDate { get; set; }

        public virtual ICollection<MovieCopyClient> ClientMoviesHistory { get; set; }
    }
}
