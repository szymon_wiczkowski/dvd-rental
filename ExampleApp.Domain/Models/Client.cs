﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExampleApp.Domain.Models
{
    public class Client
    {
        public int Id { get; set; }

        [Required]
        public string FirstName { get; set; }

        [Required]
        public string LastName { get; set; }

        public string City { get; set; }

        public string PostCode { get; set; }

        public string Street { get; set; }

        [Required]
        public string Telephone { get; set; }

        public string Email { get; set; }

        public virtual ICollection<MovieCopyClient> ClientMoviesHistory { get; set; }

        [NotMapped]
        public string FullName
        {
            get
            {
                return $"{LastName} {FirstName}";
            }
        }

        [NotMapped]
        public string Address
        {
            get
            {
                return $"{City}, {Street}";
            }
        }
    }
}
