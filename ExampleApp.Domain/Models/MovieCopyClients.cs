﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExampleApp.Domain.Models
{
    public class MovieCopyClient
    {
        public int Id { get; set; }

        public int ClientId { get; set; }

        public virtual Client Client { get; set; }

        public int MovieCopyId { get; set; }

        public virtual MovieCopy MovieCopy { get; set; }

        public DateTime TakeDate { get; set; }

        public DateTime? BackDate { get; set; }
    }
}
