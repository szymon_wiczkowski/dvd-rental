﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExampleApp.Domain.Models
{
    public class Movie
    {
        public int Id { get; set; }

        [Required]
        public string Title { get; set; }

        public virtual ICollection<MovieCopy> MovieCopies { get; set; }

        [NotMapped]
        public int TotalCopy
        {
            get
            {
                return MovieCopies.Count();
            }
        }

        [NotMapped]
        public int CopiesOnStock
        {
            get
            {
                return MovieCopies.Where(c => c.IsOnStock).Count();
            }
        }
    }
}
