﻿using ExampleApp.Domain.Models;
using System.Collections.Generic;

namespace ExampleApp.Application
{
    public interface IClientMoviesService
    {
        IEnumerable<MovieCopyClient> GetNotBackClientMovies(int clientId);
    }
}