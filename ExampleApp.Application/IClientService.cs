﻿using ExampleApp.Domain.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExampleApp.Application
{
    public interface IClientService
    {
        IEnumerable<Client> GetClients();

        IEnumerable<Client> GetClients(string search);
    }
}
