﻿using ExampleApp.Domain.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExampleApp.Application
{
    public interface IMovieService
    {
        IEnumerable<Movie> GetMovies();

        IEnumerable<Movie> GetMovies(string search);

        MovieCopyClient RentMovie(int movieId, int clientId);

        MovieCopyClient GetBackMovie(int movieCopyId, int clientId);
    }
}
