﻿using ExampleApp.Domain.Models;
using ExampleApp.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExampleApp.Application
{
    public class ClientMoviesService : IClientMoviesService
    {
        IMovieCopyClientRepository _movieCopyClientRepository = new MovieCopyClientRepository();

        public ClientMoviesService() { }

        public IEnumerable<MovieCopyClient> GetNotBackClientMovies(int clientId)
        {
            return _movieCopyClientRepository.Get(clientId, null);
        }
    }
}
