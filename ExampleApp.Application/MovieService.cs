﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ExampleApp.Domain.Models;
using ExampleApp.Repository;

namespace ExampleApp.Application
{
    public class MovieService : IMovieService
    {
        IMovieRepository _movieRepository = new MovieRepository();
        IClientRepository _clientRepository = new ClientRepository();

        public IEnumerable<Movie> GetMovies()
        {
            return _movieRepository.Get();
        }

        public IEnumerable<Movie> GetMovies(string search)
        {
            return _movieRepository.Get(search);
        }

        public MovieCopyClient RentMovie(int movieId, int clientId)
        {
            var movieCopy = _movieRepository.FindCopyOnStock(movieId);

            if (movieCopy == null)
                throw new Exception("no movie on stock");

            var client = _clientRepository.Get(clientId);
            movieCopy.IsOnStock = false;

            var movieCopyClient = new MovieCopyClient()
            {
                ClientId = clientId,
                MovieCopyId = movieCopy.Id,
                TakeDate = DateTime.Now,
            };

            _movieRepository.InsertMovieCopyClient(movieCopyClient);

            movieCopyClient = _movieRepository.GetClientCopy(movieCopyClient.Id);

            return movieCopyClient;
        }

        public MovieCopyClient GetBackMovie(int movieCopyId, int clientId)
        {
            var movieCopyClient = _movieRepository.GetMovieCopyClient(movieCopyId, clientId);
            movieCopyClient.BackDate = DateTime.Now;
            _movieRepository.UpdateMovieCopyClient(movieCopyClient);

            var movieCopy = _movieRepository.GetMovieCopy(movieCopyId);
            movieCopy.IsOnStock = true;
            _movieRepository.UpdateMovieCopy(movieCopy);

            return movieCopyClient;
        }
    }
}
