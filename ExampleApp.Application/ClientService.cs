﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ExampleApp.Domain.Models;
using ExampleApp.Repository;

namespace ExampleApp.Application
{
    public class ClientService : IClientService
    {
        IClientRepository clientRepository = new ClientRepository();

        public ClientService()
        {
            
        }

        public IEnumerable<Client> GetClients()
        {
            return clientRepository.Get();
        }

        public IEnumerable<Client> GetClients(string search)
        {
            return clientRepository.Get(search);
        }
    }
}
