﻿using ExampleApp.Application;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;

namespace ExampleApp.Controllers
{
    public class ClientCopiesController : ApiController
    {
        IClientMoviesService _clientMoviesService = new ClientMoviesService();

        public ClientCopiesController()
        {

        }

        [HttpGet]
        public IHttpActionResult ClientCopies(int clientId)
        {
            var movieCopies = _clientMoviesService.GetNotBackClientMovies(clientId);
            return Ok(movieCopies.Select(c => new
            {
                c.Id,
                title = c.MovieCopy.Movie.Title,
                serialNumber = c.MovieCopy.SerialNumber,
                c.MovieCopyId,
                c.ClientId
            }).OrderBy(o => o.title));
        }
    }
}