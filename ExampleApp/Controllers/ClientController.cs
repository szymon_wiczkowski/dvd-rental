﻿using ExampleApp.Application;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;

namespace ExampleApp.Controllers
{
    public class ClientController : ApiController
    {
        IClientService _clientService;

        public ClientController()
        {
            _clientService = new ClientService();
        }

        [HttpGet]
        public IHttpActionResult GetClients()
        {
            var clients = _clientService.GetClients().ToList();
            
            return Ok(clients.Select(c => new {
                c.Id,
                c.FullName,
                c.Telephone,
                c.Address
            }));
        }

        [HttpGet]
        [Route("api/listClients")]
        public IHttpActionResult GetClients(string search)
        {
            search = search.ToLower();

            var clients = _clientService.GetClients(search)
                .OrderBy(o => o.FullName);

            return Ok(clients.Select(c => new
            {
                c.Id,
                text = $"{c.FullName} (tel. {c.Telephone})",
            }));
        }

        [HttpPost]
        public IHttpActionResult UpdateClient()
        {
            return Ok();
        }

        [HttpPut]
        public IHttpActionResult AddClient()
        {
            return Ok();
        }
    }
}