﻿using ExampleApp.Application;
using ExampleApp.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;

namespace ExampleApp.Controllers
{
    public class MovieController : ApiController
    {
        IMovieService _movieService = new MovieService();

        public MovieController()
        {

        }

        [HttpGet]
        public IHttpActionResult GetMovies()
        {
            var movies = _movieService.GetMovies();
            return Ok(movies.Select(m => new
            {
                m.Id,
                m.Title,
                m.TotalCopy,
                m.CopiesOnStock,
            }));
        }

        [HttpGet]
        [Route("api/listMovies")]
        public IHttpActionResult GetMovies(string search)
        {
            search = search.ToLower();

            var movies = _movieService.GetMovies(search)
                .OrderBy(o => o.Title);

            return Ok(movies.Select(m => new
            {
                m.Id,
                text = $"{m.Title} (Total: {m.TotalCopy} Stock: {m.CopiesOnStock})",
                disabled = m.CopiesOnStock < 1,
            }));
        }

        [HttpPut]
        [Route("api/rentMovie")]
        public IHttpActionResult RentMovie(RentMovieViewModel model)
        {
            var movieCopy = _movieService.RentMovie(model.MovieId, model.ClientId);
            return Ok(
                new
                {
                    id = movieCopy.Id,
                    title = movieCopy.MovieCopy.Movie.Title,
                    serialNumber = movieCopy.MovieCopy.SerialNumber,
                    client = movieCopy.Client.FullName,
                });
        }

        [HttpPut]
        [Route("api/getBackMovie")]
        public IHttpActionResult GetBackMovie(GetBackMovieViewModel model)
        {
            var copy = _movieService.GetBackMovie(model.MovieCopyId, model.ClientId);
            return Ok(new
            {
                client = copy.Client.FullName,
                title = copy.MovieCopy.Movie.Title,
                serialNumber = copy.MovieCopy.SerialNumber,
            });
        }
    }
}