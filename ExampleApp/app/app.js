﻿(function () {
    'use strict';

    var ExampleApp = angular.module('Example', [
        'ui.router',
        'ui.bootstrap',
        'Example.ApplicationControllers',
        'Example.ClientControllers',
        'Example.ClientServices',
        'Example.MovieControllers',
        'Example.MovieServices',
    ]);

    ExampleApp.constant('VALIDATION_MESSAGES', {
        required: 'field is required',
        maxLenght: 'text is too long',
        postalCode: 'Invalid format, correct 80-257',
        illegalCharacter: 'Illegal character',
        saveSuccess: 'Well done! Save success.',
        saveFail: 'Save failed - please check logs.'
    });

    ExampleApp.constant('RES_STATUS', {
        Ok: 200,
    });

    //todo cut form view
    ExampleApp.constant('REG_EX', {
        postalCode: '/[0-9]{2}-[0-9]{3}/g',
        name: '/^[a-zA-Z]{2,}\s[a-zA-Z]{2,}$/',
        street: '/^\s*\S+(?:\s+\S+){2}/',

    });

    ExampleApp.config(['$stateProvider', '$urlRouterProvider',
    function ($stateProvider, $urlRouterProvider) {
        $stateProvider
            .state('clients', {
                url: '/clients',
                templateUrl: 'app/components/client/list.html',
                controller: 'ClientsController',
                resolve: {
                    clientService: 'clientService',
                },
            })
            .state('movies', {
                url: '/movies',
                templateUrl: 'app/components/movie/list.html',
                controller: 'MoviesController',
                resolve: {
                    movieService: 'movieService',
                },
            })
            .state('rentmovie', {
                url: '/rentmovie',
                templateUrl: 'app/components/movie/rent.html',
                controller: 'RentMovieController',
                resolve: {
                    movieService: 'movieService',
                },
                params: { movieId: null }
            })
            .state('getbackmovie', {
                url: '/getbackmovie',
                templateUrl: 'app/components/movie/getback.html',
                controller: 'GetBackMovieController',
                resolve: {
                    movieService: 'movieService',
                },
                params: { movieId: null }
            });

        $urlRouterProvider.otherwise('/rentmovie');
    }]);
})();