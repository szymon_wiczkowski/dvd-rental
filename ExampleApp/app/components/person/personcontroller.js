﻿(function () {
    var exampleApp = angular.module('Example.PersonControllers', []);

    exampleApp.controller('AddPersonController', function ($scope, personService, VALIDATION_MESSAGES, REG_EX, RES_STATUS) {
        $scope.messages = VALIDATION_MESSAGES;
        $scope.regex = REG_EX;
        $scope.model = {};
        $scope.persons = [];
        $scope.saveSuccess = false;
        $scope.alerts = [];

        $scope.submitPersonForm = function (isValid) {
            $scope.submitted = true;

            if (!isValid)
                return false;

            $scope.persons.push($scope.model);
            $scope.submitted = false;
            $scope.model = {};
        }

        $scope.closeAlert = function (index) {
            $scope.alerts.splice(index, 1);
        };

        $scope.logIt = function () {
            personService.batchPersonsUpdate($scope.persons).then(
                function (status) {
                    if (status == RES_STATUS.Ok) {
                        $scope.persons = [];
                        $scope.alerts = [];
                        $scope.alerts.push({ type: 'success', msg: $scope.messages.saveSuccess })
                    }
                    else {
                        $scope.alerts = [];
                        $scope.alerts.push({ type: 'danger', msg: $scope.messages.saveFail })
                    }
                });
        }
    });
})();