﻿(function () {
    'use strict';

    var exampleApp = angular.module("Example.PersonServices", []);

    exampleApp.factory('personService', function ($http, $log) {
        return {
            batchPersonsUpdate: function (model) {
                var url = 'api/persons';

                return $http.post(url, model)
                    .then(function (res) {
                        return res.status;
                    }).catch(
                    function (error) {
                        $log.error('Failed for batchPersonsUpdate.' + error.data);
                        return error.status;
                    });
            },
        }
    });
})();