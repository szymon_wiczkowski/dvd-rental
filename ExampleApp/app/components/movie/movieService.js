﻿(function () {
    'use strict';

    var exampleApp = angular.module("Example.MovieServices", []);

    exampleApp.factory('movieService', function ($http, $log) {
        return {
            getMovies: function () {
                var url = 'api/movie';

                return $http.get(url)
                    .then(function (res) {
                        return res.data;
                    }).catch(
                    function (error) {
                        $log.error('Failed for getMovies.' + error.data);
                        return error.status;
                    });
            },

            rentMovie: function (model) {
                var url = 'api/rentMovie';

                return $http.put(url, model)
                    .then(function (res) {
                        return res.data;
                    }).catch(
                    function (error) {
                        console.log(error.data);
                        $log.error('Failed for rentMovie.' + error.data);
                        return error.status;
                    });
            },

            getBackMovie: function (model) {
                var url = 'api/getBackMovie';

                return $http.put(url, model)
                    .then(function (res) {
                        return res.data;
                    }).catch(
                    function (error) {
                        console.log(error.data);
                        $log.error('Failed for getBackMovie.' + error.data);
                        return error.status;
                    });
            },

            clientCopies: function (clientId) {
                var url = 'api/clientCopies?clientId=' + clientId;

                return $http.get(url)
                    .then(function (res) {
                        return res.data;
                    }).catch(
                    function (error) {
                        $log.error('Failed for userClientCopies.' + error.data);
                        return error.status;
                    });
            },

        }
    });
})();