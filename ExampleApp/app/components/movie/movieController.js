﻿(function () {
    var exampleApp = angular.module('Example.MovieControllers', []);

    exampleApp.controller('MoviesController', function ($scope, $state, movieService) {
        $scope.movies = [];

        movieService.getMovies().then(function (data) {
            $scope.movies = data;
        });

        $scope.rent = function (movie) {
            $state.go('rentmovie', {
                movieId: movie.Id,
            });
        }
    });

    exampleApp.controller('RentMovieController', function ($scope, $stateParams, movieService) {
        $scope.model = {};
        $scope.alertMsg = '';

        if ($stateParams.movieId != null && $stateParams.movieId) {
            $scope.movieId = $stateParams.movieId;
        }

        $scope.closeAlert = function () {
            $scope.alertMsg = '';
        }

        $scope.rent = function () {
            movieService.rentMovie($scope.model).then(function (res) {
                $scope.alertMsg = 'Give ' + res.client + ' movie \'' + res.title + '\' copy serial number \'' + res.serialNumber + '\'';
                $scope.model.clientId = '';
                $scope.model.movieId = '';
            });
        }

        angular.element(document).ready(function () {
            $("#moviesList").select2({
                ajax: {
                    url: "api/listMovies",
                    dataType: 'json',
                    delay: 250,
                    data: function (params) {
                        return {
                            search: params.term,
                        };
                    },
                    processResults: function (data, params) {
                        return {
                            results: data
                        };
                    },
                    cache: false
                },
                escapeMarkup: function (markup) { return markup; }, // let our custom formatter work
                minimumInputLength: 1,
            });

            $("#clientsList").select2({
                ajax: {
                    url: "api/listClients",
                    dataType: 'json',
                    delay: 250,
                    data: function (params) {
                        return {
                            search: params.term,
                        };
                    },
                    processResults: function (data, params) {
                        return {
                            results: data
                        };
                    },
                    cache: false
                },
                escapeMarkup: function (markup) { return markup; }, // let our custom formatter work
                minimumInputLength: 1,
            });
        });
    });
    exampleApp.controller('GetBackMovieController', function ($scope, $stateParams, movieService) {
        $scope.model = {};
        $scope.clientMoviesCopy = [];
        $scope.alertMsg = '';

        $scope.clientChanged = function (clientId) {
            movieService.clientCopies(clientId).then(function (data) {
                $scope.clientMoviesCopy = data;
            });
        }

        $scope.getBack = function () {
            movieService.getBackMovie($scope.model).then(function (res) {
                $scope.alertMsg = 'Client ' + res.client + ' back movie \'' + res.title + '\', copy serial number \'' + res.serialNumber + '\'';
                $scope.model.clientId = '';
                $scope.model.movieCopyId = '';
                $scope.clientMoviesCopy = [];
            });
        }

        $scope.closeAlert = function () {
            $scope.alertMsg = '';
        }

        angular.element(document).ready(function () {
            $("#clientsList").select2({
                ajax: {
                    url: "api/listClients",
                    dataType: 'json',
                    delay: 250,
                    data: function (params) {
                        return {
                            search: params.term,
                        };
                    },
                    processResults: function (data, params) {
                        return {
                            results: data
                        };
                    },
                    cache: false
                },
                escapeMarkup: function (markup) { return markup; }, // let our custom formatter work
                minimumInputLength: 1,
            });
        });
    });
})();