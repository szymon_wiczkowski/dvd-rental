﻿(function () {
    var exampleApp = angular.module('Example.ClientControllers', []);

    exampleApp.controller('ClientsController', function ($scope, clientService, VALIDATION_MESSAGES, REG_EX, RES_STATUS) {
        $scope.clients = [];

        clientService.getClients().then(function (data) {
            $scope.clients = data;
        });
    });
})();