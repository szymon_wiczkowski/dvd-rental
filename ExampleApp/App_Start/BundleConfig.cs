﻿using System.Web.Optimization;

namespace CodeRepository.Web
{
    public class BundleConfig
    {
        // For more information on bundling, visit http://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new StyleBundle("~/Content/css").Include(
                "~/Content/bootstrap.css",
                "~/Content/bootstrap-theme.css",
                "~/Content/ui-bootstrap-csp.css",
                "~/Content/css/select2.min.css"));

            bundles.Add(new ScriptBundle("~/bundles/libs").Include(
                "~/Scripts/jquery-{version}.js",
                "~/Scripts/angular.js",
                "~/Scripts/angular-ui-router.min.js",
                "~/Scripts/angular-animate.js",
                "~/Scripts/bootstrap.js",
                "~/Scripts/select2.min.js",
                "~/Scripts/angular-ui/ui-bootstrap.js",
                "~/Scripts/angular-ui/ui-bootstrap-tpls.js"
                ));

            bundles.Add(new ScriptBundle("~/bundles/app").Include(
                "~/app/components/person/personcontroller.js",
                "~/app/components/person/personservice.js",
                "~/app/components/application/applicationControllers.js",
                "~/app/components/client/clientController.js",
                "~/app/components/client/clientService.js",
                "~/app/components/movie/movieController.js",
                "~/app/components/movie/movieService.js",
                "~/app/app.js"
                ));
        }
    }
}