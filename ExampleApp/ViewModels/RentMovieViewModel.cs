﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ExampleApp.ViewModels
{
    public class RentMovieViewModel
    {
        public int MovieId { get; set; }

        public int ClientId { get; set; }
    }
}