﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ExampleApp.ViewModels
{
    public class GetBackMovieViewModel
    {
        public int ClientId { get; set; }

        public int MovieCopyId { get; set; }
    }
}