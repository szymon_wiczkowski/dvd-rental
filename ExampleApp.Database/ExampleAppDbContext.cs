﻿using ExampleApp.Domain.Models;
using System;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;

namespace ExampleApp.Database
{
    public class ExampleAppDbContext : DbContext
    {
        public DbSet<Client> Clients { get; set; }

        public DbSet<Movie> Movies { get; set; }

        public DbSet<MovieCopy> MoviesCopies { get; set; }

        public DbSet<MovieCopyClient> MoviesCopyClients { get; set; }

        public ExampleAppDbContext() : base("ExampleAppContext")
        {

        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();
            modelBuilder.Properties<DateTime>().Configure(c => c.HasColumnType("datetime2"));

            base.OnModelCreating(modelBuilder);
        }
    }
}
