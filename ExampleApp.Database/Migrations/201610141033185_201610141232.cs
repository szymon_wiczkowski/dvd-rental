namespace ExampleApp.Database.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class _201610141232 : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Client",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        FirstName = c.String(nullable: false),
                        LastName = c.String(nullable: false),
                        City = c.String(),
                        PostCode = c.String(),
                        Street = c.String(),
                        Telephone = c.String(nullable: false),
                        Email = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.MovieCopyClients",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        ClientId = c.Int(nullable: false),
                        MovieCopyId = c.Int(nullable: false),
                        TakeDate = c.DateTime(nullable: false, precision: 7, storeType: "datetime2"),
                        BackDate = c.DateTime(precision: 7, storeType: "datetime2"),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Client", t => t.ClientId, cascadeDelete: true)
                .ForeignKey("dbo.MovieCopy", t => t.MovieCopyId, cascadeDelete: true)
                .Index(t => t.ClientId)
                .Index(t => t.MovieCopyId);
            
            CreateTable(
                "dbo.MovieCopy",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        MovieId = c.Int(nullable: false),
                        SerialNumber = c.String(nullable: false),
                        BuyDate = c.DateTime(nullable: false, precision: 7, storeType: "datetime2"),
                        TrashDate = c.DateTime(precision: 7, storeType: "datetime2"),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Movie", t => t.MovieId, cascadeDelete: true)
                .Index(t => t.MovieId);
            
            CreateTable(
                "dbo.Movie",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Title = c.String(nullable: false),
                        TotalCopy = c.Int(nullable: false),
                        CopiesOnStock = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.MovieCopy", "MovieId", "dbo.Movie");
            DropForeignKey("dbo.MovieCopyClients", "MovieCopyId", "dbo.MovieCopy");
            DropForeignKey("dbo.MovieCopyClients", "ClientId", "dbo.Client");
            DropIndex("dbo.MovieCopy", new[] { "MovieId" });
            DropIndex("dbo.MovieCopyClients", new[] { "MovieCopyId" });
            DropIndex("dbo.MovieCopyClients", new[] { "ClientId" });
            DropTable("dbo.Movie");
            DropTable("dbo.MovieCopy");
            DropTable("dbo.MovieCopyClients");
            DropTable("dbo.Client");
        }
    }
}
