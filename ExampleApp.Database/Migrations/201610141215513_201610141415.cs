namespace ExampleApp.Database.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class _201610141415 : DbMigration
    {
        public override void Up()
        {
            DropIndex("dbo.MovieCopy", new[] { "SerialNumber" });
            AlterColumn("dbo.MovieCopy", "SerialNumber", c => c.String(nullable: false, maxLength: 100));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.MovieCopy", "SerialNumber", c => c.String(nullable: false));
            CreateIndex("dbo.MovieCopy", "SerialNumber", unique: true);
        }
    }
}
