namespace ExampleApp.Database.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class _201610141416 : DbMigration
    {
        public override void Up()
        {
            CreateIndex("dbo.MovieCopy", "SerialNumber", unique: true);
        }
        
        public override void Down()
        {
            DropIndex("dbo.MovieCopy", new[] { "SerialNumber" });
        }
    }
}
