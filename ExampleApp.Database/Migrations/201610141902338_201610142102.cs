namespace ExampleApp.Database.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class _201610142102 : DbMigration
    {
        public override void Up()
        {
            RenameTable(name: "dbo.MovieCopyClients", newName: "MovieCopyClient");
        }
        
        public override void Down()
        {
            RenameTable(name: "dbo.MovieCopyClient", newName: "MovieCopyClients");
        }
    }
}
