namespace ExampleApp.Database.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class _201610141703 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.MovieCopy", "IsOnStock", c => c.Boolean(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.MovieCopy", "IsOnStock");
        }
    }
}
