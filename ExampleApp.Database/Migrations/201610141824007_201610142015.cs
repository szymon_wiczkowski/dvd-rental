namespace ExampleApp.Database.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class _201610142015 : DbMigration
    {
        public override void Up()
        {
            DropColumn("dbo.Movie", "TotalCopy");
            DropColumn("dbo.Movie", "CopiesOnStock");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Movie", "CopiesOnStock", c => c.Int(nullable: false));
            AddColumn("dbo.Movie", "TotalCopy", c => c.Int(nullable: false));
        }
    }
}
